<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk_UA" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+14"/>
        <location line="+279"/>
        <source>Search cover</source>
        <translation>Пошук обкладинки</translation>
    </message>
    <message>
        <location line="-263"/>
        <source>Online Search</source>
        <translation>Пошук у мережі</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start search automatically</source>
        <translation>Розпочати пошук автоматично</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Text or url</source>
        <translation>Текст або url</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Search for</source>
        <translation>Шукати...</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Server</source>
        <translation>Сервер</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Automatic search</source>
        <translation>Автоматичний пошук</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Local Search</source>
        <translation>Локальний пошук</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Find covers in directory</source>
        <translation>Знайдені обкладинки в каталозі</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+299"/>
        <source>%n cover(s) found</source>
        <translation><numerusform>%n обкладинку знайдено</numerusform><numerusform>%n обкладинок знайдено</numerusform><numerusform>%n обкладинок знайдено</numerusform><numerusform>%n обкладинок знайдено</numerusform></translation>
    </message>
    <message>
        <location line="+143"/>
        <location line="+27"/>
        <source>Also save cover to %1</source>
        <translation>Зберігти обкладинку в %1</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation>Пошуковий веб-перегляд не ввімкнено</translation>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation>Діалог</translation>
    </message>
    <message>
        <location filename="../src/Gui/History/GUI_History.cpp" line="+50"/>
        <source>Load more entries</source>
        <translation>Завантажити більше записів</translation>
    </message>
    <message>
        <location line="+139"/>
        <source>Scroll to top</source>
        <translation>Прокрутити верх</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll to bottom</source>
        <translation>Прокрутити вниз</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select date range</source>
        <translation>Вибрати діапазон дат</translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation>Інформація/Зміна</translation>
    </message>
    <message>
        <location line="+274"/>
        <source>Loading files...</source>
        <translation>Завантаження файлів...</translation>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+38"/>
        <source>switch</source>
        <translation>Перемкнути</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Save Lyrics</source>
        <translation>Зберегти текст пісні</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+314"/>
        <source>Save lyrics not supported</source>
        <translation>Не вдається зберегти текст</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Overwrite lyrics</source>
        <translation>Перезаписати текст пісні</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save lyrics</source>
        <translation>Зберегти текст пісні</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>Джерело</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>Імпорт</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>Імпортувати треки в бібліотеку</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Select target folder</source>
        <translation>Вибрати каталог</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+123"/>
        <source>Loading tracks</source>
        <translation>Завантаження треків</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>Немає треків</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Importing</source>
        <translation>Імпортування</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>Завершено</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Rollback</source>
        <translation>Повернути</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>Відмінено</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Choose target directory</source>
        <translation>Вибрати цільовий каталог</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation>%1&lt;br /&gt;не каталог з бібліотекою</translation>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>Бібліотека</translation>
    </message>
    <message>
        <location line="+469"/>
        <source>Directory does not exist</source>
        <translation>Каталог не існує</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+376"/>
        <source>Audio files</source>
        <translation>Аудіо файли</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.ui" line="+29"/>
        <source>Module</source>
        <translation>Модулі</translation>
    </message>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+279"/>
        <source>Cannot open file</source>
        <translation>Не вдалося відкрити файл</translation>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+273"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation>#Потоки</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>Якість</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation>Бітрейт</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-162"/>
        <source>Audio Converter</source>
        <translation>Аудіо конвертер</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation>Потоки</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+121"/>
        <source>Cannot find encoder</source>
        <translation>Неможливо знайти шифрування</translation>
    </message>
    <message>
        <location line="-109"/>
        <location line="+13"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>Плейлист не містить треків, які підтримуються конвертером</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>No track will be converted.</source>
        <translation>Жодного треку не буде конвертовано</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>These tracks will be ignored</source>
        <translation>Ці треки будуть проігноровані</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Choose target directory</source>
        <translation>Вибрати цільовий каталог</translation>
    </message>
    <message numerus="yes">
        <location line="+30"/>
        <source>Failed to convert %n track(s)</source>
        <translation><numerusform>% трек неможливо конвертувати</numerusform><numerusform>% треків неможливо конвертувати</numerusform><numerusform>% треків неможливо конвертувати</numerusform><numerusform>% треків неможливо конвертувати</numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation>Будь ласка, перевірте файли журналу</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation>Всі треки можуть бути конвертовані</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>Успішно завершено</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation>Повторення</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation>Не знайдено жодної закладки</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation>На жаль, закладки можуть бути створені лише для треків бібліотеки.</translation>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>Трансляція неможлива</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>Заборонити</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>Заборонити всі</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation><numerusform>%n прослуховувач</numerusform><numerusform>%n прослуховувачів</numerusform><numerusform>%n прослуховувачів</numerusform><numerusform>%n прослуховувачів</numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation>Передача трансляції на порт %1 неможлива</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>Можливо, цей порт використовується іншим додатком ?</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation>Зберігти висоту тону</translation>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation>Висота тону</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 і %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation>%1 не знайдено</translation>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>Стиль</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation>Діапазон</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation>Верт. розташування</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation>Висота прямокутника</translation>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation>Гориз. розташування</translation>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation>Налаштування переходу</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>Рівень</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation>Ширина прямокутника</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation>Верт. розташування</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>Налаштування стилю</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>Колір 2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>Колір 1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>Колір 3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>Колір 4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>Вибрати цільовий плейлист</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation>відносні шляхи до файлу</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>Зберегти плейлист як...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation>Пошук радіостанції</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+59"/>
        <source>Show radio stations from %1 to %2</source>
        <translation>Показати радіостанції з %1 до %2</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Country</source>
        <translation>Країна</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>Запитати дозвіл</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation>Порт %1 вже використовується</translation>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation>Неактивний</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Активний</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation>Видалити всі обкладинки з бази даних</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation>Очистити кеш</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation>Видалити всі обкладинки із каталога Sayonara</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>Видалити файли</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation>Зберігти знайдені обкладинки в каталогі бібліотеки, де розташовані аудіофайли</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation>Зберігти знайдені обкладинки в бібліотеці </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation>Отримати відсутні обкладинки з інтернету</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation>Збереження обкладинок в базі даних призводить до значно швидшого доступу, але це в свою чергу призводить до створення більшої бази даних</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation>Зберігти знайдені обкладинки в базі даних</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation>Зберігти знайдені обкладинки в каталозі Sayonara</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation>Ім’я обкладинки</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation>Ім’я обкладинки файлу</translation>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+24"/>
        <source>English</source>
        <translation>Англійська</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Check for update</source>
        <translation>Перевірити оновлення</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Import new language</source>
        <translation>Імпортувати нову мову</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>For new languages I am always looking for translators</source>
        <translation>Для нових мов я активно шукаю перекладачів</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+62"/>
        <source>Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Cannot check for language update</source>
        <translation>Неможливо перевірити на оновлення мови</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Language is up to date</source>
        <translation>Мова оновлена</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation>Неможливо отримати оновлення мови</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation>Мова успішно оновлена</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>The language file could not be imported</source>
        <translation>Мовний файл неможливо імпортувати</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The language file was imported successfully</source>
        <translation>Мовний файл був успішно імпортований</translation>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+37"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>Ввійти</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Scrobble time</source>
        <translation>Час скроблінгу</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Username</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+162"/>
        <source>Logged in</source>
        <translation>Вхід виконан</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>Вхід до системи не виконано</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>Бібліотеки</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation>Інтеракція бібліотеки-плейлиста</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation>Drag and Drop: Перемістити в плейлист</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation>залишити без змін</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation>розпочати відтворення у разі зупинки і якщо плейлист порожній</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>Подвійний натиск: створити новий плейлист і </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation>розпочати відтворення у разі зупинки</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation>розпочати відтворення негайно</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation>(це ігнорується, якщо плейлист знаходиться в режимі &quot;Шукати до кінця&quot;)</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>Інше</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation>Показати кнопку &quot;Очистити виділене&quot;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation>Ігнорувати англійській артикль &quot;The&quot; в назві виконавця</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>Неможливо змінити бібліотеку </translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation>Затримка (мс)</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>Повідомлення</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+17"/>
        <source>Hide instead of close</source>
        <translation>Натомість закрити, сховати додаток</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Start hidden</source>
        <translation>Розпочати в прихованому режимі</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Update notifications</source>
        <translation>Повідомлення про оновлення</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show system tray icon</source>
        <translation>Показати піктограму системного лотка</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+108"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation>Це може привести до того, що Sayonara більше не з&apos;явиться.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation>У цьому випадку, при наступному запуску використовуйте параметр &apos;--how&apos;.</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation>Налаштування запуску</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation>Завантажити тимчасові плейлисти</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation>Завантажити збережені плейлисти</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>Розпочати відтворення</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>Вибрати останній відтворюваний трек</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation>Запам&apos;ятати час відтворення останнього треку</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation>Зупинити режим</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation>Після натиску стоп, завантажити останній трек</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation>Подивитися</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Show covers</source>
        <translation>Показати обкладинки</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show numbers</source>
        <translation>Показати нумерацію</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Show rating</source>
        <translation>Показати рейтинг</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show clear button</source>
        <translation>Показати кнопку очищення</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&apos;italic text&apos;</source>
        <translation>&apos;текст курсивом&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>Приклад</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation>*жирний текст*</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Show footer</source>
        <translation>Показати колонтитул</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation>Перегляд списка відтворення: Неправильний вираз</translation>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+150"/>
        <source>Preferences</source>
        <translation>Налаштування</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+33"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save username/password</source>
        <translation>Зберігти ім&apos;я користувача/ пароль</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Host</source>
        <translation>Вузол</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Active</source>
        <translation>Активний</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>Автоматичний пошук</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Username</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>Проксі-сервер</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation>Виявленно через UDP</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation>Дистанційне управління URL</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation>UDP порт</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controllable</source>
        <translation>Якщо активовано, Sayonara відповість на запит UDP про те, що він може управляти дистанційно</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation>Дистанційне управління</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation>Порт %1 вже використовується</translation>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>Приклад</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation>Ігнорувати написання з вел і мал літер</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation>Параметр</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation>Ігнорувати гравіс</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation>Ігнорувати спеціальні символи</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation>Натиснути shortcut</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation>Shortcuts</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation>Знайдено подвійну комбінацію клавіш</translation>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>Загальне</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Create session directory</source>
        <translation>Створити каталог сесії</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Automatic recording</source>
        <translation>Автоматичний запис</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Target directory</source>
        <translation>Цільовий каталог</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Session Directory</source>
        <translation>Каталог сеансу</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Choose available placeholders</source>
        <translation>Вибрати доступні заповнювачі</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation>Шаблон шляху</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>Приклад</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+173"/>
        <source>Choose target directory</source>
        <translation>Вибрати цільовий каталог</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Target directory is empty</source>
        <translation>Цільовий каталог пустий</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>Будь ласка, виберіть інший каталог</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation>Неможливо створити %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation>Шаблон шляху не є чинним</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation>Запис потоку</translation>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation>мс</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation>Розмір буфера</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>Показати історію</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation>Відкрити потік у новій вкладці</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation>%1 і %2</translation>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+14"/>
        <location line="+34"/>
        <source>Edit style sheet</source>
        <translation>Змінити таблицю стилю</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation>Темний інтерфейс</translation>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>Розмір шрифту</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>Жирний</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>Найменування шрифту</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation>Стандарт</translation>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation>Застосувати вибрану піктограму в темному стилі</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+91"/>
        <source>Icons</source>
        <translation>Піктограми</translation>
    </message>
    <message>
        <location line="+90"/>
        <location line="+63"/>
        <source>System theme</source>
        <translation>Стандарт</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation>Налаштування обкладинки</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation>Змінити таблицю стилей</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>Інтерфейс користувача</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>Загальне</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation>Показати велику обкладинку</translation>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+31"/>
        <source>Shutdown</source>
        <translation>Завершення роботи</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Shutdown after playlist finished</source>
        <translation>Після останнього трека завершити роботу комп&apos;ютера</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>minutes</source>
        <translation>хвилин</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after</source>
        <translation>Завершення роботи комп&apos;ютер після</translation>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+135"/>
        <source>Donate to Soma.fm</source>
        <translation>Пожертвувати Soma.fm</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation>Пошук в Soundcloud</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Search artist</source>
        <translation>Шукати виконавця</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>Бібліотека</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation>Пошук за назвами,виконавцями та альбомами</translation>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>Замінити</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>Оригінал</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation>Файл не містить обкладинки</translation>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation>Подробиці</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation>Файл існує</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation>Доступно до змін</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation>Деякі файли неможливо зберегти</translation>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+43"/>
        <source>Discnumber</source>
        <translation>Номер диска</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation>Виконавець альбому</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>Коментувати</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>всі</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation>ID3 тег добути зі шляху файла </translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation>Файли доступні лише для читання</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Undo all</source>
        <translation>Повернути все</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+160"/>
        <source>Load complete album</source>
        <translation>Завантажити альбом повністю</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Metadata</source>
        <translation>Метадата</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation>Тег із цільового файлу</translation>
    </message>
    <message numerus="yes">
        <location line="+91"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation><numerusform>Неможливо застосувати вираз до %n треку</numerusform><numerusform>Неможливо застосувати вираз до %n треків</numerusform><numerusform>Неможливо застосувати вираз до %n треків</numerusform><numerusform>Неможливо застосувати вираз до %n треків</numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>Ігнорувати ці треки?</translation>
    </message>
    <message>
        <location line="+398"/>
        <source>All changes will be lost</source>
        <translation>Всі зміни будуть втрачені</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation>Вираз</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>CD Ном</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>Застосувати до всього</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>Мітка</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>Будь ласка, виберіть спочатку текст</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+223"/>
        <source>%1 wants to listen to your music.</source>
        <translation>%1 бажає долучитись до прослуховування вашої музики.</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+845"/>
        <source>All %1 could be removed</source>
        <translation>Всі %1 видалені</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation>%1 із %2 %3 не можуть бути видалені</translation>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+227"/>
        <source>Cannot import tracks</source>
        <translation>Неможливо імпортувати треки</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>Всі файли можуть імпортуватись</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation>%1 із %2 файлів можуть бути імпортовані</translation>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+220"/>
        <source>Looking for covers</source>
        <translation>Шукаю обкладинки</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Reading files</source>
        <translation>Читаю файли</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation>Видаляю зниклі треки</translation>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation>Неможливо скачати текст з %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation>Текст не знайдено</translation>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation>Комп&apos;ютер виключиться після завершення поточного плейлиста</translation>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation><numerusform>Комп&apos;ютер завершить роботу через %n хвилин</numerusform><numerusform>Комп&apos;ютер завершить роботу через %n хвилин</numerusform><numerusform>Комп&apos;ютер завершить роботу через %n хвилин</numerusform><numerusform>Комп&apos;ютер завершить роботу через %n хвилин</numerusform></translation>
    </message>
</context>
<context>
    <name>LastFM::Base</name>
    <message>
        <location filename="../src/Components/Streaming/LastFM/LastFM.cpp" line="+151"/>
        <source>Cannot login to Last.fm</source>
        <translation>Не вдається увійти до Last.fm</translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>Веб сторінка</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+255"/>
        <source>Permalink Url</source>
        <translation>Постійне посилання</translation>
    </message>
    <message>
        <location line="-242"/>
        <source>Followers/Following</source>
        <translation>Followers</translation>
    </message>
    <message>
        <location line="+92"/>
        <location line="+154"/>
        <source>Purchase Url</source>
        <translation>Придбати на</translation>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+65"/>
        <source>Search an alternative cover</source>
        <translation>Пошук альтернативної обкладинки</translation>
    </message>
</context>
<context>
    <name>Gui::DoubleCalendarDialog</name>
    <message>
        <location filename="../src/Gui/History/DoubleCalendarDialog.cpp" line="+44"/>
        <source>Select date range</source>
        <translation>Вибрати діапазон дат</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start date</source>
        <translation>Дата початку</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>End date</source>
        <translation>Кінцева дата</translation>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation>Історія</translation>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+105"/>
        <source>Write cover to tracks</source>
        <translation>Зберігти обкладинку в файлі трека</translation>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+212"/>
        <source>Toolbar</source>
        <translation>Панель інструментів</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+211"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation>Використовувати Ctrl + mouse для збільшення</translation>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+303"/>
        <source>Rename by metadata</source>
        <translation>Перейменувати за метаданними</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation>Згорнути все</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation>Перемістити до іншої бібліотеки</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation>Копіювати до іншої бібліотеки</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>View in file manager</source>
        <translation>Переглянути в файловому менеджері</translation>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+408"/>
        <source>Copy here</source>
        <translation>Копіювати сюди</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>Пересунути сюди</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+187"/>
        <source>Could not create directory</source>
        <translation>Неможливо створити каталог</translation>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+110"/>
        <source>Updating genres</source>
        <translation>Оновити жанри</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation>Ви дійсно бажаєте вилучити %1? з усіх треків?</translation>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation>Будь ласка, виберіть ім&apos;я для своєї бібліотеки</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation>Будь ласка, виберіть ім&apos;я для своєї бібліотеки</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation>Ім&apos;я файлу недійсне</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation>Бібліотека з таким же шляхом файлу вже існує</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation>Бібліотека, яка містить цей шлях до файлу, вже існує</translation>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+222"/>
        <source>Resize columns</source>
        <translation>Змінити розмір стовпців</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resize columns automatically</source>
        <translation>Автоматично змінити розмір стовпців</translation>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+143"/>
        <source>kBit/s</source>
        <translation>kBit/s</translation>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+88"/>
        <source>Only from library</source>
        <translation>Тільки з бібліотеки</translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>You are about to delete %n file(s)</source>
        <translation><numerusform>Ви збираєтесь видалити %n файл</numerusform><numerusform>Ви збираєтесь видалити %n файлів</numerusform><numerusform>Ви збираєтесь видалити %n файлів</numerusform><numerusform>Ви збираєтесь видалити %n файлів</numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+67"/>
        <source>Fast scan</source>
        <translation>Швидке сканування</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>Детальне сканування</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Only scan for new and deleted files</source>
        <translation>Сканувати тільки нові і видалені файли</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Scan all files in your library directory</source>
        <translation>Сканувати всі файли у каталозі бібліотеки</translation>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+157"/>
        <source>Statistics</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation>Змінити бібліотеку</translation>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+51"/>
        <location line="+59"/>
        <source>Merge</source>
        <translation>Об’єднати</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+72"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation>Автор %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>Авторські права</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+348"/>
        <source>View</source>
        <translation>Вигляд</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+15"/>
        <source>Help</source>
        <translation>Допомога</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Plugins</source>
        <translation>Плагіни</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Show large cover</source>
        <translation>Показати велику обкладинку</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>На весь екран</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Media files</source>
        <translation>Медіа файли</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Відкрити медіа файли</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>For bug reports and feature requests please visit Sayonara&apos;s project page at GitLab</source>
        <translation>Щоб отримати звіти про помилки або надіслати запити про функції, будь ласка, відвідайте сторінку проекту Sayonara в GitLab</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>Про Sayonara</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation>Автор %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>Ліцензія</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation>Пожертвувати</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thanks to all the brave translators and to everyone who helps building Sayonara packages</source>
        <translation>Щиро дякую  всім перекладачам і кожному, хто допомає створювати Sayonara пакети</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>And special thanks to those people with local music collections</source>
        <translation>І особлива подяка користувачам з локальною музичною колекцією</translation>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+128"/>
        <source>Current song</source>
        <translation>Поточна пісня</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+72"/>
        <source>A new version is available!</source>
        <translation>Доступна нова версія!</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+252"/>
        <source>Playlist empty</source>
        <translation>Плейлист порожній</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>Медіа файли</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Відкрити медіа файли</translation>
    </message>
    <message>
        <location line="+206"/>
        <source>Playlist name already exists</source>
        <translation>Ім&apos;я плейлиста вже існує</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+194"/>
        <source>Please set library path first</source>
        <translation>Будь ласка, задайте спочатку шлях до бібліотеки</translation>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+244"/>
        <source>Please set library path first</source>
        <translation>Будь ласка, задайте спочатку шлях до бібліотеки</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation>Відмінити завершення?</translation>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation>Перейти до поточного треку</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation>Показати трек в бібліотеці</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation>Режим відтворення файлів</translation>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+501"/>
        <source>Goto row</source>
        <translation>Перейти до рядка</translation>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation>Кросфейдер</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 і %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation>Кросфейдер не працює з Alsa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation>Безперервне відтворення не працює з Alsa</translation>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+119"/>
        <location line="+47"/>
        <source>Linked sliders</source>
        <translation>Зв`язані регулятори</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Equalizer</source>
        <translation>Еквалайзер</translation>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>Рівень</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation>Спектрограма</translation>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation>Діапазон</translation>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation>Налаштування ще не збережені&lt;br /&gt;Зберегти?</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>Будь ласка, вкажіть ім&apos;я</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>Зберегти зміни?</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>Не знайдено жодного плейлиста</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation>Неможливо перейменувати плейлист</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation>Недопустиме ім&apos;я</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation>Ви дійсно хочете видалити %1?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation>Неможливо видалити плейлист %1</translation>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+244"/>
        <source>Cannot open stream</source>
        <translation>Потік не можливо відкрити</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Please choose another name</source>
        <translation>Будь ласка, виберіть інше ім’я</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation>Ви дійсно бажаєте видалити %1</translation>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation>Подкаст</translation>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation>Пошук радіостанції</translation>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation>Аудіо</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation>Комбінація клавіш Enter</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation>Комбінація клавіш вже використовується </translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation>Тест</translation>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+125"/>
        <source>Cannot fetch stations</source>
        <translation>Неможливо завантажити радіостанції</translation>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+84"/>
        <source>Query too short</source>
        <translation>Занадто короткий запит</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>No artists found</source>
        <translation>Не знайдено жодного виконавця</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation><numerusform>Знайдено %n виконавця</numerusform><numerusform>Знайдено %n виконавців</numerusform><numerusform>Знайдено %n виконавців</numerusform><numerusform>Знайдено %n виконавців</numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation><numerusform>%n плейлист знайдено</numerusform><numerusform>%n плейлистів знайдено</numerusform><numerusform>%n плейлистів знайдено</numerusform><numerusform>%n плейлистів знайдено</numerusform></translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>Відтворити у новій вкладці</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation>Стандартний вигляд</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation>Переглянути обкладинку</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation>Вигляд каталогу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>Панель інструментів</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation>Перегляд треків відображається, якщо перегляд треків містить треки з різними типами файлів</translation>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+55"/>
        <source>Image files</source>
        <translation>Медіа файли</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation>Немає файлів</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation>Відкрити файли зображень</translation>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+164"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation>Запис потоку</translation>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation>Shortcuts</translation>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation>Стрілка вгору</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation>Попередній результат пошуку</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation>Стрілка вниз</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>Наступний результат пошуку</translation>
    </message>
</context>
<context>
    <name>Gui::LineEdit</name>
    <message>
        <location filename="../src/Gui/Utils/Widgets/LineEdit.cpp" line="+133"/>
        <source>Hint: Use up and down arrow keys for switching between upper and lower case letters</source>
        <translation>Підказка: Використовуйте клавіші зі стрілками вгору та вниз для перемикання між великими та малими літерами</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove special characters (letters)</source>
        <translation>Видалити спеціальні символи (літери)</translation>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>Про додаток</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Дія</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>Дії</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation>Активувати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Активний</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add artist</source>
        <translation>Додати виконавця</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>Додати нову вкладку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>Альбом</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artist</source>
        <translation>Виконавець альбому</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation>Виконавці альбому</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>Альбоми</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation>Всі</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation>Прикріпити треки до списку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation>Додаток</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Застосувати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation>Виконавець</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation>Виконавці</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation>За зростанням</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation>Автоматично</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation>Бітрейт</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>Закладки</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>Трансляція</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation>за</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Відмінити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>Не вдалося знайти MP3 інструмент кодування</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>Очистити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation>Очистити виділене</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation>Закрити інші</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>Закрити вкладку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>Коментувати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>Продовжити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation>Обкладинки</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation>Створений</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation>Створити новий каталог</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation>Створити нову бібліотеку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>Темний інтерфейс</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation>Дні</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation>д</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation>Стандарт</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+88"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Descending</source>
        <translation>За спаданням</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>Каталог</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>Каталоги</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation>CD Ном</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>Тривалість</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation>Тривал.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation>Динамічне відтворення</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>Змінити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation>Порожній вхід</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation>Ввести ім&apos;я</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation>Будь ласка, введіть нове ім&apos;я</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation>Ввести URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation>Запис</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation>Записи</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation>Швидко</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>Назва файла</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>Файли</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>Розмір файлу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>Тип файлу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>Фільтр</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation>1-й</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>Шрифти</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation>Повний текст</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation>Безпер. відтвор.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>GB</source>
        <translation>ГБ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation>Жанр</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation>Жанри</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation>Сховати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation>Годин</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation>Імпорт каталогу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation>Імпорт файлів</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>Неактивний</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation>Інфо</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>Завантаження</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation>Завантаження %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation>Некоректні символи</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>KB</source>
        <translation>кБ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+f</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Вкладка</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>Бібліотека</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation>Шлях до бібліотеки</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation>Тип вигляду бібліотеки</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation>Слухати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation>Live пошук</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation>Логгер</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Log level</source>
        <translation>Рівень журналу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation>Текст пісні</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>MB</source>
        <translation>Мбайт</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Мінімізувати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation>Хвилин</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation>хв</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation>Відсутній</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation>Змінено</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation>Місяці</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>Вимкнути звук</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>Звук увімкнено</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>Нова</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation>Наступна сторінка</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>Наступний трек</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>Не покращувати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation>Немає альбомів</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>Треки</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>Пересунути вниз</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>Пересунути вгору</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>ОК</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation>в</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Відкрити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>Відкрити каталог</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>Відкрити файл</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>або</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation>Перезаписати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>Відтворити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>Час відтворення</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>Відтворити у новій вкладці</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>Список відтворення</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>Списки відтворення</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>Відтворити наступним</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation>Старт/Пауза</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation>Плагін</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation>Подкасти</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation>Попередня сторінка</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>Попередній трек</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation>Придбати на</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>Вийти</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation>Радіо</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation>Радіостанція</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>Рейтинг</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation>Дійсно</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>Перезавантажити бібліотеку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>Вилучити зі списку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>Перейменувати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>Повторити 1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>Повторити все</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>Замінити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>Відновити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>Повторити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation>Зворотній порядок</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation>Шаблон</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation>Випадкове відтворення</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>Вимкнути</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation>Зберегти як</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation>Зберегти у файл</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation>Сканування аудіофайлів</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation>Шукати наступним</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation>Шукати попередній результат пошуку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation>2-й</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation>Секунд</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation>с</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation>Перемотати назад</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation>Перемотати вперед</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation>Показати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation>Показати виконавців альбому</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation>Показати обкладинки</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>Показати бібліотеку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>Подібні виконавці</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>Критерій впорядковування</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation>Веб-потоки</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation>Веб-потік URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation>Успішно виконано</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation>й</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation>3-й</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation>Трек</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation>Номер треку</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation>Трек на</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation>У вигляді дерева</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation>Невідомий альбом</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation>Невідомий виконавець</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation>Невідомий заголовок</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation>Невідомий жанр</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation>Невідомий заповнювач</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation>Невідомий рік</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation>Різне</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation>Різні альбоми</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation>Різні виконавці</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation>Різні треки</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>Зменшити гучність</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>Збільшити гучність</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation>Попередження</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation>Тижні</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation>Рік</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation>Роки</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Так</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation>Немає каталогів</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation><numerusform>%n каталог</numerusform><numerusform>%n каталогів</numerusform><numerusform>%n каталогів</numerusform><numerusform>%n каталогів</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation>Немає файлів</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation><numerusform>%n файл</numerusform><numerusform>%n файлів</numerusform><numerusform>%n файлів</numerusform><numerusform>%n файлів</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation>Немає плейлистів</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation><numerusform>%n плейлист</numerusform><numerusform>%n плейлистів</numerusform><numerusform>%n плейлистів</numerusform><numerusform>%n плейлистів</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>Немає треків</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation><numerusform>%n трек</numerusform><numerusform>%n треків</numerusform><numerusform>%n треків</numerusform><numerusform>%n треків</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation>Треки не знайдено</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation><numerusform>%n трек знайдено</numerusform><numerusform>%n треків знайдено</numerusform><numerusform>%n треків знайдено</numerusform><numerusform>%n треків знайдено</numerusform></translation>
    </message>
</context>
</TS>